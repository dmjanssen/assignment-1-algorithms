import numpy as np
import typing
import unittest
import random

from .quatrominos import Quatrominos
from .test_quatrominos import TileFactory, GameStateFactory


class PrivateGameMaker:

    @staticmethod
    def get_big_board_different_tiles() -> Quatrominos:
        player0 = {
            TileFactory.tile1212, TileFactory.tile1113,
            TileFactory.tile3333, TileFactory.tile1411,
        }
        player1 = {
            TileFactory.tile1414, TileFactory.tile2222,
            TileFactory.tile2224, TileFactory.tile1145,
        }
        board = np.full((5, 5, 4), -1)
        game_state = Quatrominos(player0, player1, board, 0)
        return game_state

    @staticmethod
    def get_big_board_more_tiles() -> Quatrominos:
        player0 = {
            TileFactory.tile1212, TileFactory.tile1113,
            TileFactory.tile3333, TileFactory.tile1411,
            TileFactory.tile1414, TileFactory.tile2222,
            TileFactory.tile2224, TileFactory.tile1145,
        }
        player1 = {
            TileFactory.tile1414, TileFactory.tile2222,
            TileFactory.tile2224, TileFactory.tile1145,
            TileFactory.tile1212, TileFactory.tile1113,
            TileFactory.tile3333, TileFactory.tile1411,
        }
        board = np.full((5, 5, 4), -1)
        game_state = Quatrominos(player0, player1, board, 0)
        return game_state

    @staticmethod
    def get_board_with_random_tiles() -> Quatrominos:
        player0, player1 = set(), set()

        for i in range(3):
            player0.add((random.randint(0, 5), random.randint(0, 5), random.randint(0, 5), random.randint(0, 5)))
            player1.add((random.randint(0, 5), random.randint(0, 5), random.randint(0, 5), random.randint(0, 5)))

        board = np.full((5,5,4), -1)
        game_state = Quatrominos(player0, player1, board, 1)

        return game_state

    @staticmethod
    def get_board_with_tiles_for_greedy_test():
        player0 = {
            (0, 5, 2, 0), (4, 3, 1, 4)
        }
        player1 = {
            (5, 0, 3, 0), (3, 4, 0, 5)
        }

        board = np.full((3, 3, 4), -1)
        game_state = Quatrominos(player0, player1, board, 0)

        return game_state



class TestPrivateQuatrominos(unittest.TestCase):

    def test_win_big_different_tiles(self):
        game_state = PrivateGameMaker.get_big_board_different_tiles()
        self.assertTrue(game_state.current_player_can_win())

    def test_win_big_more_tiles(self):
        game_state = PrivateGameMaker.get_big_board_more_tiles()
        self.assertTrue(game_state.current_player_can_win())

    def test_random_tiles(self):
        game_state = PrivateGameMaker.get_board_with_random_tiles()
        result = game_state.current_player_can_win()

        # Because it is random it could possibly result in a loss
        # We want to have this test, because it adds a random element to your testing
        # All the other tests are fixed
        self.assertTrue(result)

    def test_greedy_approach(self):
        game_state = PrivateGameMaker.get_board_with_tiles_for_greedy_test()
        result = game_state.best_move_greedy()

        self.assertEqual((1, 1, (3, 1, 4, 4)), result)



