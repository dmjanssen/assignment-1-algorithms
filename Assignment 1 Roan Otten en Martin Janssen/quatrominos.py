import numpy as np
import typing
import math


class Quatrominos(object):

    def __init__(self, player0: typing.Set[typing.Tuple[int, int, int, int]],
                 player1: typing.Set[typing.Tuple[int, int, int, int]],
                 board: np.ndarray,
                 player_on_turn: int):
        """
        Initializes the game board, and divides the tiles among both players.
        Each tile is represented as a 1d numpy array, consisting of exactly
        four numbers, where the first number (index 0) is positioned north, the
        second number (index 1) is positioned east, the third number (index 2)
        is positioned south and finally the fourth number (index 3) is
        positioned west. Of course, both players are free to rotate the numbers
        both clockwise and anti-clockwise.

        :param player0: The tiles that are in the hand of player 0
        :param player1: the tiles that are in the hand of player 1
        :param board: The playing board will be a 3-dimensional array, where
        the first two dimensions depict the columns and rows of the game board,
        respectively, and the third dimension is of size 4, representing a
        tile.
        :param player_on_turn: 0 iff player 0 is on turn, 1 otherwise
        """
        self.player_hand = [player0, player1]
        self.board = board
        self.player_on_turn = player_on_turn

        self.print_current_state()

    #    ______   ____    __    ____ .__   __.     _______  __    __  .__   __.   ______ .___________. __    ______   .__   __.      _______.
    #   /  __  \  \   \  /  \  /   / |  \ |  |    |   ____||  |  |  | |  \ |  |  /      ||           ||  |  /  __  \  |  \ |  |     /       |
    #  |  |  |  |  \   \/    \/   /  |   \|  |    |  |__   |  |  |  | |   \|  | |  ,----'`---|  |----`|  | |  |  |  | |   \|  |    |   (----`
    #  |  |  |  |   \            /   |  . `  |    |   __|  |  |  |  | |  . `  | |  |         |  |     |  | |  |  |  | |  . `  |     \   \
    #  |  `--'  |    \    /\    /    |  |\   |    |  |     |  `--'  | |  |\   | |  `----.    |  |     |  | |  `--'  | |  |\   | .----)   |
    #   \______/      \__/  \__/     |__| \__|    |__|      \______/  |__| \__|  \______|    |__|     |__|  \______/  |__| \__| |_______/
    #

    def print_current_state(self) -> None:
        """
        Prints the current state, in free format

        First prints the tiles in the hands of both players
        Then prints the board in a nice way
        """
        player = 0
        for hand in self.player_hand:
            print('Hand player ', player)
            player += 1
            self.print_player_hand(hand)

        self.print_board(self.board)

    def print_player_hand(self, hand) -> None:
        """
        Prints the hand of the player using the horizontal printer function
        :param hand: Hand of the player
        :return: None
        """
        self.horizontal_printer(hand)

    def print_board(self, board) -> None:
        """
        Prints the board using the horizontal printer function
        :param board: Board
        :return: None
        """
        print('Board')

        for row in board:
            # print(row)
            self.horizontal_printer(row)

    @staticmethod
    def horizontal_printer(row) -> None:
        """
        Prints the hand or board in a horizontal way, this looks really nice
        and gives a graphical aspect to the 3D array
        :param row: Items that you want to print horizontally
        :return: None
        """
        string = ''

        # Add top lines
        string += '+---+ ' * len(row) + '\n'

        for item in row:
            string += f'| {item[0] if item[0] != -1 else " "} | '
        string += '\n'

        for item in row:
            string += f'|{item[3] if item[0] != -1 else " "} {item[1] if item[0] != -1 else " "}| '
        string += '\n'

        for item in row:
            string += f'| {item[2] if item[0] != -1 else " "} | '
        string += '\n'

        # Add bottom line
        string += '+---+ ' * len(row)

        print(string)

    def get_all_tiles_on_board(self) -> typing.List:
        """
        Get all tiles on the board that are not empty
        :return: List of non-empty tiles
        """
        tiles = []
        for i in range(len(self.board)):
            for j in range(len(self.board[0])):
                if self.board[i][j][0] != -1:
                    tiles.append([i, j, self.board[i][j]])

        return tiles

    def get_all_empty_spots_on_board(self) -> typing.List[typing.Tuple]:
        """
        Gets all the tiles on the board that are empty
        :return: List of empty tiles
        """
        tiles = []
        for i in range(len(self.board)):
            for j in range(len(self.board[0])):
                if self.board[i][j][0] == -1:
                    tiles.append((i, j))

        return tiles

    def get_middle_of_board(self) -> typing.Tuple:
        """
        Get the middle tile of the current board
        :return: Tuple with (y,x) coordinate which are the same ofcoure
        """
        return tuple((math.floor(len(self.board) / 2), math.floor(len(self.board[0]) / 2)))

    def is_empty_position(self, y, x) -> bool:
        """
        Check if the position is empty
        :param y: Y coordinate of position
        :param x: X coordinate of position
        :return: True if empty, false if filled
        """
        return self.board[y][x][0] == -1

    def get_adjacent_tiles_for_position(self, y, x):
        """
        Returns all the adjacent tiles for a coordinate (y,x)
        :param y: Y coordinate of tile
        :param x: X coordinate of tile
        :return: top, right, bottom, left tile
        """
        top = self.board[y - 1][x] if (y > 0) else None
        right = self.board[y][x + 1] if (x < (len(self.board[0]) - 1)) else None
        bottom = self.board[y + 1][x] if (y < (len(self.board) - 1)) else None
        left = self.board[y][x - 1] if (x > 0) else None
        return top, right, bottom, left

    def delete_tile_from_hand(self, tile):
        """
        Deletes tile from current hand in all orientations
        :param tile: tile object
        :return: true if deleted, with the specific tile orientation. False if not found in hand
        """
        for i in range(4):
            if self.get_rotated_tile(tile, i) in self.player_hand[self.player_on_turn]:
                self.player_hand[self.player_on_turn].remove(self.get_rotated_tile(tile, i))
                return True, self.get_rotated_tile(tile, i)

        return False, None

    def add_tile_to_hand(self, tile) -> bool:
        """
        Adds tile to current hand
        :param tile: Tile object
        :return: True if successfully added
        """
        self.player_hand[self.player_on_turn].add(tile)
        return True

    @staticmethod
    def has_enough_neighbours(tiles) -> bool:
        """
        Checks for enough neighbours.
        On a board there needs to be at least one neighbour, otherwise the move is not legal
        :param tiles: List of tile objects
        :return:
        """
        count = 0
        for tile in tiles:
            if tile is not None:
                if tile[0] != -1:
                    count += 1
        return count > 0

    @staticmethod
    def match_tiles(orientation, tile_1, tile_2):
        """
        Matches tiles in a certain orientation
        :param orientation: top, right, bottom or left string
        :param tile_1: first tile object
        :param tile_2: second tile object
        :return: True if there is a match, else False
        """
        match = False

        if tile_1 is None or tile_2 is None:
            return True
        elif tile_1[0] == -1:
            return True

        if orientation == 'top':
            num_1 = 2
            num_2 = 0
        elif orientation == 'right':
            num_1 = 3
            num_2 = 1
        elif orientation == 'bottom':
            num_1 = 0
            num_2 = 2
        else:
            num_1 = 1
            num_2 = 3

        if tile_1[num_1] == tile_2[num_2]:
            match = True

        return match

    #    _______ .______          ___       _______   _______  _______      _______  __    __  .__   __.   ______ .___________. __    ______   .__   __.      _______.
    #   /  _____||   _  \        /   \     |       \ |   ____||       \    |   ____||  |  |  | |  \ |  |  /      ||           ||  |  /  __  \  |  \ |  |     /       |
    #  |  |  __  |  |_)  |      /  ^  \    |  .--.  ||  |__   |  .--.  |   |  |__   |  |  |  | |   \|  | |  ,----'`---|  |----`|  | |  |  |  | |   \|  |    |   (----`
    #  |  | |_ | |      /      /  /_\  \   |  |  |  ||   __|  |  |  |  |   |   __|  |  |  |  | |  . `  | |  |         |  |     |  | |  |  |  | |  . `  |     \   \
    #  |  |__| | |  |\  \----./  _____  \  |  '--'  ||  |____ |  '--'  |   |  |     |  `--'  | |  |\   | |  `----.    |  |     |  | |  `--'  | |  |\   | .----)   |
    #   \______| | _| `._____/__/     \__\ |_______/ |_______||_______/    |__|      \______/  |__| \__|  \______|    |__|     |__|  \______/  |__| \__| |_______/
    #

    @staticmethod
    def get_rotated_tile(tile: typing.Tuple[int, int, int, int],
                         rotations: int) -> typing.Tuple[int, int, int, int]:
        """
        Returns a tile that is a clock-wise rotation of the input tile. E.g.,
        tile (1, 2, 3, 4) will in case of a single rotation be rotated to
        tile (4, 1, 2, 3)

        :param tile: the input tile to rotate
        :param rotations: the number of rotations (each rotation being a 90
        degree clockwise turn)
        :return: The rotated tile
        """
        l = list(tile)
        for i in range(rotations):
            lastOne = l.pop()
            l.insert(0, lastOne)

        return tuple(l)

    def adjacent_locations(self) -> typing.Set[typing.Tuple[int, int]]:
        """
        Returns a set with tuples of (y,x)-coordinates where we could
        potentially fit a tile (if the numbers would match). Note that the
        first tile should always be placed in the middle.

        :return: a set with tuples of (y,x)-coordinates of vacant positions
        adjacent to non-vacant positions
        """
        tiles = self.get_all_tiles_on_board()
        all_empty_tiles = self.get_all_empty_spots_on_board()

        adjacent_tiles = []

        # If there are any tiles on the board
        if len(tiles):
            for tile in tiles:
                for empty_tile in all_empty_tiles:
                    # If either the x or y coordinate is the same and the distance is 1
                    if empty_tile[0] == tile[0] and abs(empty_tile[1] - tile[1]) == 1:
                        adjacent_tiles.append(empty_tile)
                    elif empty_tile[1] == tile[1] and abs(empty_tile[0] - tile[0]) == 1:
                        adjacent_tiles.append(empty_tile)
        # No tiles, so return the middle of the board
        else:
            adjacent_tiles.append(self.get_middle_of_board())

        return set(adjacent_tiles)

    def can_place_given_tile(self, board_y: int, board_x: int,
                             tile: np.array) -> bool:
        """
        Checks whether the tile, in its current orientation, can be placed on
        the indicated position on the board. Note that the numbers of the tile
        are north, east, south, west, respectively.

        :param board_y: board y index to place the tile
        :param board_x: board x index to place the tile
        :param tile: the numpy array representing the tile
        :return: true if the tile can be placed in this orientation on the
        board, false otherwise
        """
        if self.is_empty_position(board_y, board_x):
            top, right, bottom, left = self.get_adjacent_tiles_for_position(board_y, board_x)
            correct = 0

            # Check all orientations
            if top is not None:
                if top[2] == tile[0] or top[0] == -1:
                    correct += 1
            else:
                correct += 1

            if right is not None:
                if right[3] == tile[1] or right[0] == -1:
                    correct += 1
            else:
                correct += 1

            if bottom is not None:
                if bottom[0] == tile[2] or bottom[0] == -1:
                    correct += 1
            else:
                correct += 1

            if left is not None:
                if left[1] == tile[3] or left[0] == -1:
                    correct += 1
            else:
                correct += 1

            # print(correct)
            return correct == 4

        else:
            return False

    def count_available_moves(self, tiles: np.array, allInfo = False):
        """
        Counts the number of moves that can be made, with the tiles provided.
        Note that a tile can be placed in various orientations. Different
        orientations count as different moves.

        :param tiles: A numpy array with the tiles
        :param allInfo: if true returns all the possible options as a list of tuples
        :return: The number of moves a player can make
        """
        moves = 0
        positions = []

        empty_positions = self.get_all_empty_spots_on_board()

        # Board is fully empty
        if len(empty_positions) == (len(self.board) * len(self.board[0])):
            if allInfo:
                middle = self.get_middle_of_board()
                positions = [(middle[0], middle[1], self.get_rotated_tile(tile, i)) for tile in tiles for i in range(4)]
                return 4 * len(tiles), positions
            return 4*len(tiles)

        for y, x in empty_positions:

            for tile in tiles:
                top, right, bottom, left = self.get_adjacent_tiles_for_position(y, x)

                # Check for valid position
                if self.has_enough_neighbours([top, right, left, bottom]):
                    for i in range(4):
                        rot_tile = self.get_rotated_tile(tile, i)
                        # Try to match tile in the four different positions
                        if self.match_tiles('top', top, rot_tile) \
                                and self.match_tiles('right', right, rot_tile) \
                                and self.match_tiles('bottom', bottom, rot_tile) \
                                and self.match_tiles('left', left, rot_tile):

                            moves += 1
                            if (y, x, rot_tile) not in positions:
                                positions.append((y, x, rot_tile))
        # print(moves)
        if not allInfo:
            return moves
        return moves, positions

    def check_current_player_lost(self) -> bool:
        """
        Determines whether the player that is currently on turn has lost the
        game. That can either happen by the other player having played all
        their tiles, or the current player having no available moves.

        :return: True if the current player has lost, False otherwise
        """
        if self.player_on_turn == 0:
            if len(self.player_hand[1]) == 0 or self.count_available_moves(self.player_hand[0]) == 0:
                return True
        else:
            if len(self.player_hand[0]) == 0 or self.count_available_moves(self.player_hand[1]) == 0:
                return True
        return False

    def current_player_can_win(self) -> bool:
        """
        Uses a exhaustive search algorithm to determine which player will win,
        if both players adopt an optimal strategy. Use a recursive function.
        See the slides of lecture 3 to find pseudo-code for this algorithm.
        Ensure that after this function, all class variables that were changed
        are set back to their original values.

        :return: True iff the player on turn can win
        """
        # If the other player has no cars anymore or current player has no moves
        # This player can't win 
        if  self.check_current_player_lost():
            return False

        # Get moves
        possible_moves, positions = self.count_available_moves(self.player_hand[self.player_on_turn], True)

        for move in positions:

            # Delete tile from current hand
            deleted, tile_deleted = self.delete_tile_from_hand(move[2])

            if deleted:
                # Do move
                self.board[move[0], move[1]] = move[2]

                # Change player
                self.player_on_turn = 0 if self.player_on_turn == 1 else 1

                # Recursive check of moves
                if not self.current_player_can_win():
                    # Re add tile, and undo move
                    self.add_tile_to_hand(tile_deleted)
                    self.board[move[0], move[1]] = np.array([-1, -1, -1, -1])
                    return True

                self.add_tile_to_hand(tile_deleted)
                self.board[move[0], move[1]] = np.array([-1, -1, -1, -1])
            # Move not possible
            else:
                return True

        # Apparently we can't win
        return False

    def best_move_greedy(self) -> typing.Tuple[int, int, np.array]:
        """
        OPTIONAL. Design a greedy function to determine the best way. This
        algorithm involves enumerating all possible moves, and determining
        which of the moves seems good, without looking further ahead. A logical
        greedy approach would be, e.g., select the move that leaves the other
        player with the least possible amount of free moves.

        :return: A 3-tuple, containing the (y, x) coordinate of the tile, and
        the tile in its proper orientation
        """
        moves, positions = self.count_available_moves(self.player_hand[self.player_on_turn], True)
        max = 100000000
        move_to_go_with = None
        for move in positions:

            # Do move
            self.board[move[0], move[1]] = move[2]

            # Get moves for opponent after our move
            moves, positions = self.count_available_moves(self.player_hand[0 if self.player_on_turn == 1 else 1], True)

            # If this causes more trouble for opponent, do it
            if moves < max:
                move_to_go_with = move

            # Undo move
            self.board[move[0], move[1]] = np.array([-1, -1, -1, -1])

        # Do best greedy move and remove tile from hand
        self.board[move_to_go_with[0], move_to_go_with[1]] = move_to_go_with[2]
        self.delete_tile_from_hand(move_to_go_with[2])

        return move_to_go_with
